package com.rfp365.service;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rfp365.util.CardUtil;


@Service
public class DeckService {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	public static final String NOT_ENOUGH_CARDS = "Not enough cards in the deck";	
	public static final String ZERO_AMOUNT = "Zero cards were requested";
	
    String[] SUITS = { "Clubs", "Diamonds", "Hearts", "Spades" };
    String[] RANKS = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace" };    

    /**
     * Creates a deck of cards, shuffles the cards and then deals the cards our per the below arguments
     * @param numberOfPlayers: This is the total number of players that are playing
     * @param numberOfCards: This is the total number of cards that should be dealt in a hand
     * @return JSON object of the flattened map. Map consists of key:Player, value:List<Cards>
     * @throws JsonProcessingException
     */
    public String dealCards( int numberOfPlayers, int numberOfCards ) throws JsonProcessingException {
	    // initialize deck
	    int totalCards = SUITS.length * RANKS.length;
	    String[] deck = new String[totalCards];
	    for (int i = 0; i < RANKS.length; i++) {
	        for (int j = 0; j < SUITS.length; j++) {
	            deck[SUITS.length*i + j] = "   " + RANKS[i] + " of  " + SUITS[j];
	        }
	    }
	
	    // shuffle
	    deck = shuffle( totalCards, deck );
	    
	    // deal the cards
	    if ( numberOfPlayers * numberOfCards < 52 && numberOfPlayers * numberOfCards != 0 ) {	    	
		    Map<String, List<String>> hands = dealCards( numberOfPlayers, numberOfCards, deck );
		    
		    //obtain winner
		    String winner = getWinner( hands );
		    
		    //rebuild map with winner
		    List<String> winningPlayer = new ArrayList<>();
		    winningPlayer.add( winner );	    
		    hands.put("Winner", winningPlayer);
		    
	        return new ObjectMapper().writeValueAsString( hands );
	    } else {
	    	if ( numberOfPlayers * numberOfCards == 0 ) {
	    		return ZERO_AMOUNT;
	    	} else {
	    		return NOT_ENOUGH_CARDS;
	    	}
	    }
    }
    
    /**
     * Method will shuffle the deck
     * @param totalCards: This is the total amount of cards in the deck
     * @param deck: This is the String[] of the cards
     * @return String[] of the deck
     */
    private String[] shuffle ( int totalCards, String[] deck ) {
	    for (int i = 0; i < totalCards; i++) {
	        int random = i + (int) (Math.random() * (totalCards-i));
	        String temp = deck[random];
	        deck[random] = deck[i];
	        deck[i] = temp;
	    }
	    return deck;
    }
    
    /**
     * Method will deal the cards for the # cards for the # of players
     * @param numberOfPlayers: Total number of players
     * @param numberOfCards: Total number of cards per hand
     * @param deck: String[] of the deck
     * @return Map<String, List<String>> where key is the player and value is the list of cards
     * @throws JsonProcessingException
     */
    private Map<String, List<String>> dealCards( int numberOfPlayers, int numberOfCards, String[] deck ) throws JsonProcessingException {
	    Map<String, List<String>> hands = new HashMap<>();
	    for (int player=1; player <= numberOfPlayers; player++) {
	    	int deckCard = player;
	    	List<String> cards = new ArrayList<>();
	    	for ( int card=0; card < numberOfCards; card++ ) {
	    		cards.add( deck[deckCard-1] );
	    		deckCard = deckCard + numberOfPlayers;
	    	}
		    hands.put( "Player:"+Integer.toString(player), cards );
	    }	
	    log.info("total elements in hands map = " + hands.size());
        return hands;    	
    }
    
    /**
     * Method obtains the winning player
     * @param hands Map<String, List<String>>
     * @return The name of the winning player
     */
    private String getWinner( Map<String, List<String>> hands ) {
    	int[] totals = new int[hands.size()];
    	String[] players = new String[hands.size()];
    	int tracker = 0;
    	
    	for (Map.Entry<String, List<String>> entry : hands.entrySet()) {
    		int total = 0;    		
    		
    		for (String value : entry.getValue()) {
    			String firstCharacter = value.substring( 0, 1 );    			
    			total = total + CardUtil.getCardValue(firstCharacter);    			
    		} 
    		players[tracker] = entry.getKey();
    		totals[tracker] = total;
    		tracker++;
    	} 
    	   
    	return players[ CardUtil.getIndexOfMaxArrayValue(totals) ];    	
    }   
}

