package com.rfp365.util;

public class CardUtil {

	/**
	 * Method gets numeric value of the card
	 * @param value: the first letter/number of the card
	 * @return the numeric value of the card
	 */
	public static int getCardValue( String value ) {
	   switch ( value ) {
	   		case "A":   return 1;
			case "2":   return 2;
			case "3":   return 3;
			case "4":   return 4;
			case "5":   return 5;
			case "6":   return 6;
			case "7":   return 7;
			case "8":   return 8;
			case "9":   return 9;
			case "10":  return 10;
			case "J":   return 11;
			case "Q":   return 12;
			case "K":   return 13;
			default:  return 0;
       }
	}
	
	/**
	 * Gets the max index based on the values from the array.
	 * @param array
	 * @return the index for the max value
	 */
	public static int getIndexOfMaxArrayValue( int[] array) {
		int largest = array[0], index = 0;
		for (int i = 1; i < array.length; i++) {
			if ( array[i] > largest ) {
			      largest = array[i];
			      index = i;
			}
		}
		return index;
	}
}
