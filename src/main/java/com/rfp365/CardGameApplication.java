package com.rfp365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication( exclude = {JacksonAutoConfiguration.class, JmxAutoConfiguration.class})
@Configuration
@EnableAutoConfiguration
@ComponentScan("com.rfp365")
public class CardGameApplication extends WebMvcConfigurerAdapter  {

	public static void main(String[] args) {
		SpringApplication.run(CardGameApplication.class, args);
	}
	
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("/resources/**")
                    .addResourceLocations("/resources/");
    }	
	
	@Bean
	public FilterRegistrationBean deRegisterHiddenHttpMethodFilter( HiddenHttpMethodFilter filter) {
		FilterRegistrationBean bean = new FilterRegistrationBean( filter );
		bean.setEnabled( Boolean.FALSE );
		return bean;
	}
}
