package com.rfp365.controller;

import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rfp365.service.DeckService;

@RestController
public class RestfulController {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	/**
	 * Rest call that shuffles and deals the deck of cards per the parameters
	 * @param numberOfPlayers
	 * @param numberOfCards
	 * @return JSON Flattened
	 */
	@RequestMapping(value = "/dealCards", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
	public String dealCard( @RequestParam("players") int numberOfPlayers,
							@RequestParam("cards" ) int numberOfCards) {
		String json = "No Results Found";		
		DeckService deck = new DeckService();
		
		log.info("Param Number of Players = " + numberOfPlayers);
		log.info("Param Number of Cards = " + numberOfCards);
		
	    try {
			json = deck.dealCards( numberOfPlayers, numberOfCards );
		} catch (JsonProcessingException e) {
			log.warn("Error occured " + e);
		}
	    return json;
	}	
}
