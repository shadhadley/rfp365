package com.rfp365.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonProcessingException;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DeckService.class)
public class DeckServiceTest {
	
	int numberOfPlayers;
	int numberOfCards;
		   
	@Test
	public void dealCardsSuccess() {
		DeckService deck = new DeckService();
		String result = null;
		
		try {
			result = deck.dealCards(6, 2);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		assertTrue(result != null);
	}
	
	@Test
	public void dealCardsLargeNumbers() {
		DeckService deck = new DeckService();
		String result = null;
		
		try {
			result = deck.dealCards(10, 10);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		assertTrue(result.equals(DeckService.NOT_ENOUGH_CARDS));
	}	

	@Test
	public void dealCardsZero() {
		DeckService deck = new DeckService();
		String result = null;
		
		try {
			result = deck.dealCards(0, 0);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		assertTrue(result.equals(DeckService.ZERO_AMOUNT));
	}		
}
